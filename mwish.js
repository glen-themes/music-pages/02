// sleepy

var root = document.documentElement;
var rootGET = getComputedStyle(root); // follow with: getPropertyValue()
var rootSET = root.style; // follow with: setProperty()

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

document.addEventListener("DOMContentLoaded",() => {
    var bob = document.getElementsByTagName("html")[0];
       
    var tc = rootGET.getPropertyValue("--TumblrControls-Color");
    bob.setAttribute("tumblr-controls",tc)
    
    if(customize_page){
        bob.setAttribute("customize-page","true")
    }
    
    if(on_main){
        bob.setAttribute("main-page","true")
    }
});

$(document).ready(function(){
    $(".tumblr_preview_marker___").remove();
    
    /*-------- TOOLTIPS --------*/
    $("[title]").each(function(){
        if($.trim($(this).attr("title")) !== ""){
            $(this).style_my_tooltips({
                tip_follows_cursor:true,
                tip_delay_time:0,
                tip_fade_speed:0,
                attribute:"title"
            });
        }
        
        if($(this).is("[href]")){
            if($.trim($(this).attr("href")) == ""){
                $(this).css("cursor","help")
            }
        } else {
            $(this).css("cursor","help")
        }
    })
    
    /*--------------------------*/
    
    // chad
    function chaditup(){
        $(".crdz").each(function(){
            $(this).wrapInner("<div class='crhuh'></div>");
            $(this).find(".crhuh").prepend("<div crd-dot a></div><div crd-dot b></div><div crd-dot c></div>")
        })
    }
    
    chaditup()
    
    if(!($(".crdz").length || $("a[href='glenthemes']").length)){
        $("body").append('<a class="crdz" href=\"//glenthemes.tumblr.com\" title=\"&#x2756; Melodic Wish &#x2756; by glenthemes\"></a>');
        chaditup()
    }
    
    /*--------------------------*/
    // do the feather replace thing
    $("[icon-name]").each(function(){
        var qeune = $.trim($(this).attr("icon-name"));
        if(qeune !== ""){
            qeune = qeune.replaceAll(" ","-");
            $(this).attr("icon-name",qeune);
        } else {
            $(this).remove();
        }
    })
    
    $("span[icon-name]").each(function(){
        var icozn = $(this).attr("icon-name");
        
        if($.trim($(this).text()) == ""){
            $(this).prepend("<i data-feather='" + icozn + "'></i>");
            feather.replace();
            $(this).children().unwrap();
        }
    })
    
    /*--------------------------*/
    
    $("[intro]").each(function(){
        $(this).prepend("<div bakadiv></div>");
        if($(this).find("[playlist-buttons]").length){
            $(this).find("[bakadiv]").nextUntil("[playlist-buttons]").wrapAll("<div flex></div>");
            $(this).css("justify-content","space-between")
        }
    })
    
    
    $("[back-to-blog][icon-name]").each(function(){
        var btb_ico = $(this).attr("icon-name");
        $(this).prepend("<i data-feather='" + btb_ico + "'></i>")
        feather.replace();
    })
    
    $("[intro-title], [intro-subtitle]").each(function(){
        $(this).html($.trim($(this).html()))
    })
    
    $("[intro-title] + [intro-subtitle]").each(function(){
        $(this).add($(this).prev()).wrapAll("<div feint>")
    })
    
    // if only intro-title and no sub
    $("[intro-title]").each(function(){
        if(!$(this).siblings("[intro-subtitle]").length){
            $(this).wrap("<div feint>")
        }
    })
    
    // if only intro-subtitle and no title
    $("[intro-subtitle]").each(function(){
        if(!$(this).siblings("[intro-title]").length){
            $(this).wrap("<div feint>")
        }
    })
    
    /*----- PLAYLIST BUTTONS -----*/
    // shuffle thing
    $("[shuffle-button]").each(function(){
        $(this).wrapInner("<span text></span>");
        $(this).prepend("<i class='ph ph-shuffle-angular'></i>");
        
        $(this).click(function(){
            shakethat();
            $(this).addClass("shuffle-on")
        })
    })
    
    function shakethat(){
        var songz = $("[song-row]").length;
        var randoSONG = Math.floor(Math.random()*songz);
        
        if($(this).attr("random-song") == randoSONG){
            randoSONG = randoSong + 2;
        }
        
        $("[playlist-main]").find("[song-row]").eq(randoSONG).each(function(){
            $(".song-clickt").removeClass("song-clickt");
            var yfisl = $(this).find("audio")[0];
            if(yfisl.paused){
                yfisl.play();
            }
            
            $(this).addClass("song-clickt")
        })
    }
    
    // play-all thing
    $("[play-all-button]").each(function(){
        $(this).wrapInner("<span text></span>");
        $(this).prepend("<i class='ph ph-play-circle'></i>")
        
        $(this).click(function(){
            var fdpt = $("[song-row]:first audio")[0];
            if(fdpt.paused){
                fdpt.play()
            }
            
            $(".song-clickt").removeClass("song-clickt");
            $("[song-row]:first").addClass("song-clickt");
            
            $("[shuffle-button]").removeClass("shuffle-on")
        })
    })
    
    /*--------------------------*/
    
    var mpfade = $.trim(rootGET.getPropertyValue("--Playlist-FadeIn"));
    var songcount = $("[song-row]").length;
    var ap_song1 = $.trim(rootGET.getPropertyValue("--Autoplay-First-Song"));
    var ap_nxt = $.trim(rootGET.getPropertyValue("--Autoplay-Next-Song"));
    var autoLOOP = $.trim(rootGET.getPropertyValue("--Loop-Playlist"));
    
    var mpb_type = rootGET.getPropertyValue("--Music-Buttons-Style");
    mpb_type = mpb_type.slice(1).slice(0,-1);
    
    $("[playlist-main]").each(function(){
        
    })
    
    $("[song-row]").each(function(){
        $(this).prepend("<div bootons></div>");
    })
    
    $("[bootons]").each(function(){
        if(mpb_type == "filled"){
            $(this).prepend("<i class='ph ph-play-fill xplay'></i>");
            $(this).append("<i class='ph ph-pause-fill xpause'></i>");
        } else {
            $(this).prepend("<i class='ph ph-play xplay'></i>");
            $(this).append("<i class='ph ph-pause xpause'></i>");
        }
    })
    
    $("[bootons] + [song-name]").each(function(){
        $(this).add($(this).prev()).wrapAll("<div flex></div>")
    })
    
    $("[song-row] audio").each(function(){
        var op_og = $(this).parents("[song-row]");
        
        var channel = $(this)[0];
        channel.load();
        
        // just for testing
        // $(this).attr('src','https://www.w3schools.com/html/horse.ogg')
        
        // de-dropbox
        var sjefi = $.trim($(this).attr("src"));
        if(sjefi.indexOf("dropbox.com") > -1){
            sjefi.replaceAll("www.dropbox.com","dl.dropbox.com").replaceAll("?dl=0","");
            $(this).attr("src",sjefi);
        }
        
        // generate song duration
        this.addEventListener("loadedmetadata",function(){
            var durian = this.duration;
            var MO1 = "0" + Math.floor(durian / 60);
            var SO1 = "0" + (Math.floor(durian) - MO1 * 60);
            var bigdurian = MO1.substr(-2) + "<span colon>:</span>" + SO1.substr(-2);
            $(this).after("<span song-duration>" + bigdurian + "</span>");
        });
        
        // song volume
        if($(this).is("[volume]")){
            var disvol = $.trim($(this).attr("volume"));
            if(disvol !== ""){
                disvol = parseInt(disvol) / 100;
                $(this)[0].volume = disvol;
            }
        }
        
        // when playing
        $(this).bind("play",function(){
            $(".xplay",op_og).css("opacity","0");
            $(".xpause",op_og).css("opacity","1");
            
            // stop other songs from playing when u start a new one
            $("audio").not(this).each(function(i, autres){
                autres.pause();
                autres.currentTime = 0;
                
                $(".xplay").css("opacity","1");
                $(".xpause").css("opacity","0");
                
                $(".xplay",op_og).css("opacity","0");
                $(".xpause",op_og).css("opacity","1");
            });
        })
        
        // when paused
        $(this).bind("pause",function(){
            $(".xpause",op_og).css("opacity","0");
            $(".xplay",op_og).css("opacity","1");
        })
        
        // onended
        $(this).bind("ended",function(){
            $(".xpause",op_og).css("opacity","0");
            $(".xplay",op_og).css("opacity","1");
            
            if(ap_nxt == "yes"){
                
                // IF SHUFFLE: YES
                if($(".shuffle-on").length){
                    shakethat();
                }
                
                // IF SHUFFLE: NO
                else {
                    // if autoplay-next HAS next song
                    if(op_og.next("[song-row]").length){
                        $(".song-clickt").removeClass("song-clickt")
                        op_og.next().addClass("song-clickt");
                        op_og.next().find("audio")[0].play()
                    }
                    
                    // if autoplay-next has NO next song
                    // check if user wants to loop playlist
                    // if yes, return to start
                    else {
                        if(autoLOOP == "yes"){
                            $(".song-clickt").removeClass("song-clickt");
                            $("[song-row]:first").each(function(){
                                $(this).addClass("song-clickt");
                                $(this).find("audio")[0].play()
                            })
                        }
                    }
                }//end if shuffle no
                
            }// end if ap_nxt
        })// end ondended
        
    })// end <audio> each
    
    /*----- SYNC VINYL WITH PLAY/PAUSED STATE -----*/
    // (regardless of which audio it is)
    $("audio").bind("play",function(){
        // spin vinyl when playing
        if($(".xpause").is(":visible"))
        $("[vinyl-img]").removeClass("vinpaused").addClass("vinplayin")
    })
    
    $("audio").bind("pause",function(){
        // stop spinning vinyl when paused
        $("[vinyl-img]").addClass("vinpaused");
        
        $("audio").not(this).each(function(i, autres){
            if(!autres.paused){
                $("[vinyl-img]").removeClass("vinpaused").addClass("vinplayin")
            }
        });
    })
    
    $("[song-row]").click(function(){
        var disAUDA = $(this).find("audio")[0];
        if(disAUDA.paused){
            disAUDA.play();
        } else {
            disAUDA.pause();
        }
        
        // if first load, and you click any one to play it
        if(!$(this).siblings(".song-clickt").length){
            $(this).addClass("song-clickt")
        }
        
        // if smth else has been clicked, and you click a new one
        else {
            $(this).siblings().removeClass("song-clickt");
            
            if(!$(this).hasClass("song-clickt")){
                $(this).addClass("song-clickt")
            }
        }
    })// end song click
    
    /*---- AUTOPLAY SETTINGS ----*/
    if(ap_song1 == "yes"){
        var wofww = $("[playlist-main] [song-row]:first audio")[0];
        
        if(wofww.paused){
            $("[song-row]:first").trigger("click");
        }
        
        if(customize_page){
            $("body").append("<div ap-msg><div pom>Hello! Autoplay doesn't work very well on Chrome. It will only trigger when your mouse is within the browser window whilst it's loading.</div><i class='ph ph-x'></i></div>");
            
            setTimeout(function(){
                $("[ap-msg]").css("margin-top","0");
            },269)
            
            $("[ap-msg] .ph").click(function(){
                $("[ap-msg]").css("margin-top","")
                $("[ap-msg]").delay(1000).hide(0)
            })
        }
    }//end autoplay first stuff
    
    /*----- PLAYLIST STEP FADE-IN -----*/
    function playlist_fadeIn(){
        $("[playlist-main]").css("opacity","1")
        $("[playlist-main]").attr("fadein","yes")
        $("[song-row]").each(function(TwT){
            $(this).css("animation-delay","calc(var(--Playlist-FadeIn-Step-Delay) * " + TwT + ")")
        })
    }
    
    /*------------------------------*/
    
    $("[navbar]").each(function(){
        $(this).wrapInner("<div lb-bg></div>")
    })
    
    $("[bar-title]").each(function(){
        if($.trim($(this).text()) == ""){
            $(this).remove();
        }
    })
    
    var lnksdiver = $.trim(rootGET.getPropertyValue("--Navbar-Links-Divider"));
    lnksdiver = lnksdiver.slice(1).slice(0,-1);
    
    $("[some-links] a + a").each(function(){
        $(this).before("<span divider>" + lnksdiver + "</span>")
    })
    
    /*------------------------------*/
    
    $("[rightstuff]").prepend("<div bakadiv></div>");
    
    $("[rightstuff] [bakadiv]").each(function(){
        $(this).nextUntil("[navbar]").wrapAll("<div igwsj><div lhcwt></div></div>")
    })
    
    $("[sidebar-image][vinyl='yes']").each(function(){
        $(this).append("<img vinyl-img src='https://66.media.tumblr.com/f99bc6f8f2adbd8605146e10ea4621ce/tumblr_pquw89YkQ31qg2f5co5_r2_1280.png'>")
    })
    
    /*----- VINYL LOAD-IN ANIMATION -----*/
    function vinyl_loadIn(){
        $("[vinyl-img]").each(function(){
            $(this).addClass("rollin")
        })
    }
    
    // get linksbar total height
    var bzczo = Date.now();
    var mpzui = setInterval(function(){
    	if(Date.now() - bzczo > 2000){
    		clearInterval(mpzui);
    	} else {
    	    linksbarheight();
    	}
    },0);
    
    function linksbarheight(){
        $("[navbar]").each(function(){
			if($(this).height() !== "0"){
				var lnbh = $(this).height();
				rootSET.setProperty("--Navbar-Total-Height",lnbh + "px");
			}
		})
    }
    
    $(window).on("resize",function(){
        linksbarheight()
    })
    
    /*----- CUSTOMIZE PAGE MSG [GUIDE] -----*/
    if(customize_page){
        var ntmq = $.trim(rootGET.getPropertyValue("--Show-Guide-Message"));
        
        if(ntmq == "yes"){
            $("body").eq(0).append("<div msg-prompt>Hello! Thank you for using the <b>Melodic Wish</b> music page! Please read <a href=\'//docs.google.com/presentation/d/1VPynhrrjrrgED_WfqKW4BK0QD8O96lYJB_NV82Wq_QE/edit?usp=sharing\' target=\'_blank\'>the guide</a> to get started!</div>");
            
            $("[msg-prompt]").show();
            
            setTimeout(function(){
                $("[msg-prompt]").addClass("swip")
                $("[msg-prompt]").css("margin-right","40px")
            },699)
        }
    }
    
    /*---- SHOW <BODY> ----*/
    if(on_main){
        var asbjb = Date.now();
        var nxvcd = setInterval(function(){
        	if(Date.now() - asbjb > 2200){
        		clearInterval(nxvcd);
        		
        	} else {
        	    // once things have arranged themselves,
        	    // show the things
        		if($("[igwsj]").length){
        		    $("body").css("opacity","1");
        		    
        		    if(mpfade == "yes"){
            		    playlist_fadeIn()
        		    }
        		    vinyl_loadIn();
        		}
        	}
        },0);
    }
    
    if(customize_page){
        if($("[igwsj]").length){
		    $("body").css("opacity","1");
		    
		    if(mpfade == "yes"){
    		    playlist_fadeIn()
		    }
		    vinyl_loadIn();
		}
    }
});//end ready
